import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

public class IntegerSparseMatrixSupportTest {

    private static final SparseMatrixSupport<SparseMatrix<Integer>> matrixSupport = new IntegerSparseMatrixSupport();

    @Test
    public void testToStream() {
        SparseMatrix<Integer> sparseMatrixP = toSparseMatrix(new int[][] {{5, 8, -4}, {6, 9, -5}, {4, 7, -3}});
        List<Integer> actual = matrixSupport.toStream(sparseMatrixP).collect(Collectors.toList());
        assertThat(actual, is(Arrays.asList(3, 3, 5, 6, 4, 8, 9, 7, -4, -5, -3)));
    }

    @Test
    public void testFromStream() {
        SparseMatrix<Integer> sparseMatrix = matrixSupport.fromStream(Stream.of(2, 2, 591, null, null, null));
        assertThat(sparseMatrix.getColumns(), is(2));
        assertThat(sparseMatrix.getRows(), is(2));
        assertThat(sparseMatrix.get(1, 1), is(591));
        assertThat(sparseMatrix.get(1, 2), is(nullValue()));
        assertThat(sparseMatrix.get(2, 1), is(nullValue()));
        assertThat(sparseMatrix.get(2, 2), is(nullValue()));
    }

    @Test
    public void testMultiply() {
        {
            SparseMatrix<Integer> m5x1 = matrixSupport.fromStream(Stream.of(5,
                    1,
                    0,
                    0,
                    514,
                    0,
                    0));
            SparseMatrix<Integer> m1x5 = matrixSupport.fromStream(Stream.of(1, 5,
                    0, 0, 216, 971, 891));
            assertThat(matrixSupport.toStream(matrixSupport.multiply(m5x1, m1x5)).collect(Collectors.toList()),
                    is(Arrays.asList(5, 5,
                            0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0,
                            0, 0, 111024, 499094, 457974,
                            0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0)));
            assertThat(matrixSupport.toStream(matrixSupport.multiply(m1x5, m5x1)).collect(Collectors.toList()),
                    is(Arrays.asList(1, 1, 111024)));
        }

        {
            SparseMatrix<Integer> leftMatrix2x2 = matrixSupport.fromStream(Stream.of(2, 2,
                    601, 412,
                    567, 0));
            SparseMatrix<Integer> rightMatrix2x2 = matrixSupport.fromStream(Stream.of(2, 2,
                    370, 120,
                    122, 0));
            assertThat(matrixSupport.toStream(matrixSupport.multiply(leftMatrix2x2, rightMatrix2x2)).collect(Collectors.toList()),
                    is(Arrays.asList(2, 2,
                            272634, 72120,
                            209790, 68040)));
        }

        {
            SparseMatrix<Integer> m3x2 = matrixSupport.fromStream(Stream.of(3, 2,
                    381, 920,
                    170, 0,
                    523, 154));
            SparseMatrix<Integer> m2x3 = matrixSupport.fromStream(Stream.of(2, 3,
                    62, 478, 543,
                    0, 427, 20));
            assertThat(matrixSupport.toStream(matrixSupport.multiply(m3x2, m2x3)).collect(Collectors.toList()),
                    is(Arrays.asList(3, 3,
                            23622, 574958, 225283,
                            10540, 81260, 92310,
                            32426, 315752, 287069)));
        }

        {
            SparseMatrix<Integer> m4x2 = matrixSupport.fromStream(Stream.of(4, 2,
                    2, 3,
                    5, 7,
                    11, 13,
                    17, 19));
            SparseMatrix<Integer> m2x3 = matrixSupport.fromStream(Stream.of(2, 3,
                    23, 29, 31,
                    37, 41, 43));
            assertThat(matrixSupport.toStream(matrixSupport.multiply(m4x2, m2x3)).collect(Collectors.toList()),
                    is(Arrays.asList(4, 3,
                            157, 181, 191,
                            374, 432, 456,
                            734, 852, 900,
                            1094, 1272, 1344)));
        }
    }

    private SparseMatrix<Integer> toSparseMatrix(int[][] matrix) {
        SparseMatrix<Integer> ret = new SparseMatrix<>(matrix.length, matrix[0].length);
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                ret.set(i + 1, j + 1, matrix[i][j]);
            }
        }
        return ret;
    }
}

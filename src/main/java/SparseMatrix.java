import java.util.HashMap;
import java.util.Map;

public class SparseMatrix<T extends Number> {

    private final int columns;
    private final int rows;
    private final Map<Integer, T> matrixMap;

    public SparseMatrix(int columns, int rows) {
        if (columns < 1 || rows < 1) {
            throw new IllegalArgumentException();
        }

        this.columns = columns;
        this.rows = rows;
        this.matrixMap = new HashMap<>();
    }

    public int getColumns() {
        return columns;
    }

    public int getRows() {
        return rows;
    }

    public T get(int column, int row) {
        return matrixMap.get(xyTo1D(column, row));
    }

    public void set(int column, int row, T value) {
        if (value == null) {
            matrixMap.remove(xyTo1D(column, row));
        } else {
            matrixMap.put(xyTo1D(column, row), value);
        }
    }

    private int xyTo1D(int column, int row)
    {
        if (column < 1 || column > columns)
        {
            throw new IndexOutOfBoundsException("column = " + column + " is out of bounds");
        }

        if (row < 1 || row > rows)
        {
            throw new IndexOutOfBoundsException("row = " + row + " is out of bounds");
        }

        return columns * (row - 1) + column - 1;
    }
}

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class IntegerSparseMatrixSupport implements SparseMatrixSupport<SparseMatrix<Integer>> {

    @Override
    public Stream<Integer> toStream(SparseMatrix<Integer> matrix) {
        Objects.requireNonNull(matrix);
        return Stream.generate(new MatrixStreamSupplier(matrix)).limit(matrix.getColumns() * matrix.getRows() + 2);
    }

    @Override
    public SparseMatrix<Integer> fromStream(Stream<Integer> stream) {
        Objects.requireNonNull(stream);
        MatrixStreamConsumer consumer = new MatrixStreamConsumer();
        stream.forEach(consumer);
        return consumer.getMatrix();
    }

    @Override
    public SparseMatrix<Integer> multiply(SparseMatrix<Integer> first, SparseMatrix<Integer> second) {
        Objects.requireNonNull(first);
        Objects.requireNonNull(second);

        if (first.getColumns() != second.getRows()) {
            throw new IllegalArgumentException("It is impossible!");
        }

        SparseMatrix<Integer> ret = new SparseMatrix<>(second.getColumns(), first.getRows());

        for (int row = 1; row <= ret.getRows(); row++) {
            for (int column = 1; column <= ret.getColumns(); column++) {
                Integer r = null;

                for (int k = 1; k <= first.getColumns(); k++) {
                    Integer f = first.get(k, row);
                    Integer s = second.get(column, k);

                    if (f != null && s != null) {
                        if (r == null) {
                            r = 0;
                        }

                        r += f * s;
                    }
                }

                ret.set(column, row, r);
            }
        }

        return ret;
    }

    private enum StreamState {
        START, ROWS, COLUMNS, ELEMENTS, FINISH
    }

    private static class MatrixStreamSupplier implements Supplier<Integer> {

        private final SparseMatrix<Integer> matrix;
        private StreamState state = StreamState.START;
        private int row = 1;
        private int column = 1;

        private MatrixStreamSupplier(SparseMatrix<Integer> matrix) {
            this.matrix = matrix;
        }

        @Override
        public Integer get() {
            switch (state) {
                case START:
                    state = StreamState.ROWS;
                    return matrix.getRows();

                case ROWS:
                    state = StreamState.COLUMNS;
                    return matrix.getColumns();

                case COLUMNS:
                    state = StreamState.ELEMENTS;
                    return matrix.get(column, row);

                case ELEMENTS:
                    if (column != matrix.getColumns()) {
                        return matrix.get(++column, row);
                    } else {
                        if (row != matrix.getRows()) {
                            return matrix.get(column = 1, ++row);
                        }

                        state = StreamState.FINISH;
                        return null;
                    }

                case FINISH:
                    return null;

                default:
                    throw new IllegalStateException("StreamState " + state + " is not supported");
            }
        }
    }

    private static class MatrixStreamConsumer implements Consumer<Integer> {

        private SparseMatrix<Integer> matrix;
        private int columns;
        private int rows;

        private StreamState state = StreamState.START;
        private int row = 1;
        private int column = 1;

        public SparseMatrix<Integer> getMatrix() {
            return matrix;
        }

        @Override
        public void accept(Integer value) {
            switch (state) {
                case START:
                    rows = value;
                    state = StreamState.ROWS;
                    break;

                case ROWS:
                    columns = value;
                    state = StreamState.COLUMNS;
                    matrix = new SparseMatrix<>(columns, rows);
                    break;

                case COLUMNS:
                    state = StreamState.ELEMENTS;
                    acceptElement(value);
                    break;

                case ELEMENTS:
                    if (column != matrix.getColumns()) {
                        column++;
                        acceptElement(value);
                    } else {
                        if (row != matrix.getRows()) {
                            row++;
                            column = 1;
                            acceptElement(value);
                        } else {
                            state = StreamState.FINISH;
                        }
                    }
                    break;

                case FINISH:
                    throw new IllegalStateException("Cannot consume more elements.");

                default:
                    throw new IllegalStateException("StreamState " + state + " is not supported");
            }
        }

        private void acceptElement(Integer value) {
            if (value != null) {
                matrix.set(column, row, value);
            }
        }
    }
}

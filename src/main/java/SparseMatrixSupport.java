import java.util.stream.Stream;

public interface SparseMatrixSupport<M> {

    Stream<Integer> toStream(M matrix);

    M fromStream(Stream<Integer> stream);

    M multiply(M first, M second);
}
